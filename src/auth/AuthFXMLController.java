package auth;

import cloudprint.CloudPrint;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AuthFXMLController {
    private cloudprint.CloudPrint program;
    
    public void setProgram(cloudprint.CloudPrint program){
        this.program = program;
    }
    @FXML
    private ResourceBundle resources;

    @FXML
    private Label warning;
    
    @FXML
    private ProgressIndicator wait;
    
    @FXML
    private TextField textField;
    
    @FXML
    private URL location;

    @FXML
    private Button nextButton;

    @FXML
    private RadioButton save;

    @FXML
    void nextButtonClick(ActionEvent event) throws IOException, UnknownHostException, ClassNotFoundException {
        if(warning.isVisible())
            warning.setVisible(false);
        wait.setVisible(true);
        textField.setDisable(true);
        warning.setText("Имя уже занято");
        nextButton.setDisable(true);
        save.setDisable(true);
        
        String tmp = this.textField.getText();
        this.program.setUsername(tmp);
        if(save.isSelected())
            this.program.writeSettingsToFile(program.getAddress(), program.getPort(), tmp);
        String message = program.Connect(CloudPrint.con.create);
        if(message == "false")
            setFailed();
        else if(message == "Cannot Connect! Check file settings.txt"){
            warning.setText(message);
            setFailed();
        }
        else
            program.runMainFrame(new Stage(), message);
        
        
    }
    void setFailed(){
        warning.setVisible(true);
        wait.setVisible(false);
        textField.setDisable(false);
        nextButton.setDisable(false);
        save.setDisable(false);
    }
    @FXML
    void initialize() {
        assert nextButton != null : "fx:id=\"nextButton\" was not injected: check your FXML file 'AuthFXML.fxml'.";
        assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'AuthFXML.fxml'.";

    }
}
