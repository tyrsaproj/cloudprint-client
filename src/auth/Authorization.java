/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth;

import cloudprint.PrintingAttributesWindowController;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Stas
 */
public class Authorization extends Application {
    private final cloudprint.CloudPrint main;
    private AuthFXMLController controller;
    public Authorization(cloudprint.CloudPrint main){
        this.main = main;
    }
    @Override 
    public void start(Stage primaryStage) throws IOException {
       
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AuthFXML.fxml"));
        Parent root = (Parent)loader.load();
        controller = loader.getController();
        controller.setProgram(main);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("RADICAL HYDRINTER");
        primaryStage.setResizable(false);
    }
    public AuthFXMLController getController(){
        return controller;
    }
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
