/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cloudprint;
import auth.Authorization;
import auth.AuthFXMLController;
import Print.PrintInfoContainer;
import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.PrintRequestAttributeSet;
 

/**
 *
 * @author Stas
 */


public class CloudPrint extends Application {
    
        FXMLDocumentController control;
        Authorization auth;
        String username = "null", address = "localhost";
        int port = 2222;
        //--------------------[Для принтеров и клиентов]------------------------
        static ArrayList<PrintInfoContainer> users = new ArrayList();
        //-----------------------------[END]------------------------------------
        public enum status {shutDown, tmpDisconnect};
        public enum con {create, resume};
        //-----------------------[Для Peer-to-Peer]-----------------------------
        Thread serverThread;
        //-----------------------------[END]------------------------------------
      
        Boolean isConnected = false;
        Thread IncomingReader = null;
        Socket sock;
        BufferedReader reader;
        PrintWriter writer;
        PrintRequestAttributeSet attributes;
        ObjectInputStream ois;
        ObjectOutputStream oos;
        String ip;
        File toPrint;
        String systemMessage;
        PrintInfoContainer selectedPrinter;
        public void setUsername(String name){
            this.username = name;
        }
        public int getPort(){
            return port;
        }
        public String getAddress(){
            return address;
        }
        public static  ArrayList<PrintInfoContainer> GetPrinterList(){
            return users;
        }
        public void getPrinterListFromServer() throws IOException, ClassNotFoundException{
            oos.writeObject("сom:command:getPrinter");
            oos.flush();
        }
        public void setFileToPrint(File file) {
            this.toPrint = file;
        }
        public void setPrintContainer(PrintInfoContainer selectedPrinter) {
            this.selectedPrinter = selectedPrinter;
        }
        public void ListenThread(con con){
            switch(con) {
                case create:
                    IncomingReader = new Thread(new IncomingReader());
                    IncomingReader.start();
                    break;
                case resume:
                    IncomingReader.notify();
            }

        }
        
        public void GetPrinters() throws UnknownHostException{
            PrintService[] printers = PrintServiceLookup.lookupPrintServices(null, null);
            if(printers != null)
                for(PrintService p: printers)
                    users.add(new PrintInfoContainer(p.getName(),InetAddress.getLocalHost().getHostAddress(),InetAddress.getLocalHost().getHostName()));
            
        }
        
        public void Disconnect(status st){
            try{
                String ip = InetAddress.getLocalHost().getHostAddress();
                StringBuilder sb = new StringBuilder();
                sb.append(username);
                sb.append(":");
                sb.append(ip);
                switch(st) {
                    case shutDown:
                        sb.append(":Disconnect");
                        break;
                    case tmpDisconnect:
                        IncomingReader.wait();
                        sb.append(":TDisconnect");
                        break;
            }
            oos.writeObject(sb.toString());
            oos.flush();
            sock.close();
            } 
            catch(Exception ex){
                this.control.Wazzup(ex.getLocalizedMessage());
            }
            isConnected = false;
        }    
        
        public String Connect(con con) throws UnknownHostException, IOException, ClassNotFoundException{
            String ret;
            if (!isConnected){
                username = InetAddress.getLocalHost().getHostName();
                try{
                    sock = new Socket(address, port);
                    isConnected = true;
                    GetPrinters();
                    oos = new ObjectOutputStream(sock.getOutputStream());
                    ois = new ObjectInputStream(new BufferedInputStream(sock.getInputStream()));
                    oos.writeObject(username);
                    oos.flush();
                    boolean check = ois.readBoolean();
                    if(!check){
                        sock = null;
                        return "false";
                    }
                    oos.writeObject(users);
                    oos.flush();
                    
                    Object obj;
                    obj = ois.readObject();
                    ArrayList<PrintInfoContainer> getPrinter = null; 
                    getPrinter = (ArrayList<PrintInfoContainer>) obj;
                    users.clear();
                    users.addAll(getPrinter);                   
                    String message = username + ":has connected.:Connect";
                    oos.writeObject(message);
                    oos.flush();
                    ListenThread(con);
                    ret = "Ready";
                }
                catch (Exception ex){
                     System.out.println(ex.getLocalizedMessage());
                     ret =  "Cannot Connect! Check file settings.txt";
                }
            } 
            else{
                 ret = "You are already connected. \n";
            }   
            return ret;
        }
        public void AddUnique(ArrayList<PrintInfoContainer> obj) {
            for(PrintInfoContainer p: obj) {
                if(!users.contains(p))
                    users.add(p);
            }
        }
        public void SendObject(Object obj,String command) throws IOException{
            
            oos.writeObject(this.username+":command:"+command);
            oos.writeObject(obj);
            
        }
        
        public void SendObject(String command) throws IOException{
            
            oos.flush();
            String query = this.username + ":command:" + command;
            oos.writeObject(command);
            
        }
    
        
           //--------------------------//
    
    public class IncomingReader implements Runnable{
        @Override
        public void run(){
            String[] data;
            String stream;
            try{
                while ((stream = (String)ois.readObject()) != null){
                    data = stream.split(":");
                    switch(data[2]){
                        default:
                            Platform.runLater(() -> { 
                            control.Wazzup("No Conditions were met. \n");
                            });
                            break;
                        case "setPrinter":
                            Object tmp = null;
                            ObjectInputStream get = new ObjectInputStream(new BufferedInputStream(sock.getInputStream()));
                            tmp = get.readObject();
                            if(tmp != null){
                                users.clear();
                                users.addAll((ArrayList<PrintInfoContainer>)tmp);   
                            }
                            Platform.runLater(() -> { 
                                control.RefreshPrinterList();
                                control.Wazzup("Список принтеров обновлен");    
                            });
                            System.out.println("Список принтеров получен");
                            break;
                        case "OpenServer":
                            serverThread = new Thread(new Server());
                            serverThread.start();
                            break;
                        case "OpenClient":
                            Thread clientThread = new Thread(new Client(ip));
                            clientThread.start();
                            break;
                    }
                }
           }
           catch(Exception ex) { }
        }
        
    }

    public void setIP(String ip) { 
        this.ip = ip;
    }
    public class Server implements Runnable {

        @Override
        public void run() {
            try{
                ServerSocket ss = new ServerSocket(1488);
                Socket cs = ss.accept();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            control.Wazzup(InetAddress.getLocalHost().getHostAddress() + " has connected. \n");
                        } catch (UnknownHostException ex) {
                            Logger.getLogger(CloudPrint.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                ObjectInputStream serverIn = new ObjectInputStream(new BufferedInputStream(cs.getInputStream()));
                ObjectOutputStream serverOut = new ObjectOutputStream(cs.getOutputStream());
                Object obj;
                
                
                obj  = serverIn.readObject();
                PrintRequestAttributeSet aset = (PrintRequestAttributeSet) obj;
                obj  = serverIn.readObject();
                PrintInfoContainer pCont = (PrintInfoContainer)obj;
                obj  = serverIn.readObject();
                String fileInfo = (String)obj;
                String [] data = fileInfo.split(":");
                
                byte [] array = new byte[16*1024];
                FileOutputStream fos = new FileOutputStream(data[0]);
                obj = serverIn.readObject();
                array = (byte[]) obj;
                
                fos.write(array);
                fos.close();
                

                
                PrintService tmp = this.findPrinter(pCont.getPrinter());
                if(tmp != null) {
                    File file = null;
                    this.printDocument(aset, tmp, data[0]);
                    serverOut.writeObject("Done");
                }
                else
                    serverOut.writeObject("Failed");
                ss.close();
            } catch (Exception ex) {
                Platform.runLater(() -> { 
                control.Wazzup(ex.getLocalizedMessage());
                });
            } 
        }
        private void printDocument(PrintRequestAttributeSet aset, PrintService printer, String file) throws PrintException, IOException {
            DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            DocPrintJob pj = printer.createPrintJob();
            FileInputStream fis = null;
            try{
                fis = new FileInputStream(file);
                Doc doc = new SimpleDoc(fis, flavor, null);
                pj.print(doc, aset);
            } 
            catch (Exception ex){
                System.out.print(ex.getLocalizedMessage());
            }
        }
        private PrintService findPrinter(String printerName) {
            PrintService[] allprinters = PrintServiceLookup.lookupPrintServices(null, null);
            for(PrintService p : allprinters){
                if(p.getName().equals(printerName))
                    return p;
            }
            return null;
        }
    }

    public class Client implements Runnable {

        private final String ip;
        
        public Client(String ip)
        {
            this.ip = ip;
        }
        
        @Override
        public void run() {
            try {
                Socket sk = new Socket(ip, 1488);
                ObjectOutputStream clientOut = new ObjectOutputStream(sk.getOutputStream());
                ObjectInputStream clientIn = new ObjectInputStream(new BufferedInputStream(sk.getInputStream()));
                PrintRequestAttributeSet aset = PA.GetAset();
                
                byte[] sendfile = Files.readAllBytes(toPrint.toPath());

                
                clientOut.writeObject(aset);
                clientOut.flush();
                clientOut.writeObject(selectedPrinter);
                clientOut.flush();
                String fileInfo = toPrint.getName() + ":" + toPrint.length();
                clientOut.writeObject(fileInfo);
                clientOut.flush();
                clientOut.writeObject(sendfile);
                clientOut.flush();
                Object obj = clientIn.readObject();
                systemMessage = (String)obj;
                clientOut.close();
                clientIn.close();
                sk.close();
            } catch (IOException ex) {
                Platform.runLater(() -> { 
                control.Wazzup(ex.getLocalizedMessage());
                });
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CloudPrint.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    public void showDocument(File file) {
        if(Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.open(file);
            }
            catch(Exception e) {
                System.out.print("Can't open file");
            }
        }
        else {
            System.out.print("Desktop is unsupported");
        }
    }
    
    public void writeSettingsToFile(String newHost, int newPort, String newName) {
        File file = new File("Settings.txt");
        try(BufferedWriter bufWriter = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(file)));) {
            bufWriter.write("host=" + newHost + "\n"
                        + "port=" + Integer.toString(newPort) + "\n"
                        + "name=" + newName);
        } catch(IOException ex) {
            System.out.println("Не удается получить доступ к файлу " + ex);
        }
    }
    
    private void readSettingsFromFile() {
        File file = new File("Settings.txt");
        try(BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));) {      
            
            String bufAddress = null;
            int bufPort = 0;
            String bufName = null;
            String str;
            while((str = bufReader.readLine())!= null) {
                str = str.replaceAll(" ", "");
                String[] info = str.split("=");
                if(info[0].compareTo("host") == 0 && info.length == 2) bufAddress = info[1];
                if(info[0].compareTo("port") == 0 && info.length == 2) {
                    try {
                        bufPort = Integer.parseInt(info[1]);
                    }
                    catch (NumberFormatException e) {}
                }
                if(info[0].compareTo("name")== 0 && info.length == 2) bufName = info[1];
            }
            
            if(bufAddress == null || bufName == null || bufPort == 0){
                throw new Exception("Data has an invalid format");
            } else {
                address = bufAddress;
                port = bufPort;
                username = bufName;
            }
        } catch(Exception e) {
            System.out.println(e.toString());
            writeSettingsToFile(address, port,username);
        }
        System.out.println(address);
    }    
    public void runAuth() throws IOException{
        auth = new Authorization(this);
        auth.start(new Stage());  
        
    }
    
    public static PrintingAttributesModule PA;
    public static Converter converter;
    public void runMainFrame(Stage stage,String message) throws IOException{
        PA = new PrintingAttributesModule();
        FXMLLoader load = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        Parent root = (Parent)load.load();
        control = load.getController();
        control.setProgram(this);
        this.control.Wazzup(message);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(cloudprint.CloudPrint.class.getResource("HYDRINTER.css").toExternalForm());
        stage.setTitle("RADICAL HYDRINTER");
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest((WindowEvent t)->Disconnect(status.shutDown));    
    }
    @Override
    public void start(Stage stage) throws Exception {
        readSettingsFromFile();
        if(this.username == "null")
            runAuth();
        else{
            runMainFrame(stage,Connect(CloudPrint.con.create));
        }    
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
 
}
