package cloudprint;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import static java.lang.Thread.sleep;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Converter {
    
    private static void writeSettingsToFile(String setting) {
        File file = new File("PathSettings.txt");
        try(BufferedWriter bufWriter = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(file)))) {
            bufWriter.write("OpenOfficeExeFilePath=" + setting + "\n");
        } catch(Exception e) {
            System.out.println("Не удается получить доступ к файлу " + e.toString());
        }
    }
    
    private static String readSettingsFromFile() {
        File file = new File("PathSettings.txt");
        String path = null;
        try(BufferedReader bufReader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file)))) {
            String str;
            while((str = bufReader.readLine())!= null) {
                String[] info = str.split("=");
                info[0] = info[0].replaceAll(" ", "");
                if(info[0].compareTo("OpenOfficeExeFilePath") == 0 && info.length == 2)
                    path = info[1];
            }
        } catch(Exception e) {
            System.out.println("Ошибка открытия файла настроек " + e.toString());
        }
        
        if(path == null || !(new File(path)).exists()) {
            //Открытие файлового диалога
            JFileChooser jet = new JFileChooser();
            
            jet.removeChoosableFileFilter(jet.getAcceptAllFileFilter());
            FileFilter filter = new FileNameExtensionFilter("Executable files", "exe");
            jet.setFileFilter(filter);
            
            jet.setDialogTitle("Выберите путь к файлу soffice.exe");
            int ret = jet.showDialog(null, "Открыть");
            
            if(ret == JFileChooser.APPROVE_OPTION) {
                path = jet.getSelectedFile().getAbsolutePath();
                writeSettingsToFile(path);
            }
            else
                System.out.println("Ошибка задания пути");
        }
        System.out.println(path);
        return path;
    }
    
    public static File convert(File inputFile) throws NullPointerException, IOException, InterruptedException {
        File convertedDocument =  null;
        if(inputFile.getName().endsWith(".doc") || inputFile.getName().endsWith(".docx") ||
                inputFile.getName().endsWith(".xls") || inputFile.getName().endsWith(".xlsx") || 
                inputFile.getName().endsWith(".ppt") || inputFile.getName().endsWith(".pptx")) {
            convertedDocument = convertMSODocument(inputFile);
        } else if(inputFile.getName().endsWith(".odt") || inputFile.getName().endsWith(".odg") ||
                inputFile.getName().endsWith(".odp") || inputFile.getName().endsWith(".ods")) {
            convertedDocument = convertOODocument(inputFile);
        } else {
            throw new IOException("Unsupported format");
        }
        return convertedDocument;
    }
    
    private static File convertOODocument(File inputFile) throws IOException, InterruptedException {
        Process openOfficeProcess;
        ProcessBuilder prBuilder;
        String newPath = inputFile.getAbsolutePath().substring(0, inputFile.getAbsolutePath().lastIndexOf(".")) + ".pdf";
        File outputFile = new File(newPath);
        prBuilder = new ProcessBuilder(readSettingsFromFile(), 
                "-headless", 
                "-accept=\"socket,host=127.0.0.1,port=8101;urp;\"", 
                "-nofirststartwizard");
        openOfficeProcess = prBuilder.start();
        
        try {
            OpenOfficeConnection connection = new SocketOpenOfficeConnection(8101);
            
            while(!connection.isConnected()) {
                try {
                    connection.connect();
                }
                catch(Exception e) {
                    sleep(500);
                }
            }
        
            DocumentConverter converter = new OpenOfficeDocumentConverter(connection);

            converter.convert(inputFile, outputFile);

            while(true) {
                if(outputFile.exists()) {
                    connection.disconnect();
                    return outputFile;
                }
                else {
                    sleep(500);
                }
            }
        }
        finally {
            openOfficeProcess.destroy();
        }
    }
    
    private static File convertMSODocument(File inputFile) throws IOException, InterruptedException {
        String newPath = inputFile.getAbsolutePath().substring(0, inputFile.getAbsolutePath().lastIndexOf(".")) + ".pdf";
        File emptyDocument = new File(newPath);
        ProcessBuilder prBuilder = new ProcessBuilder("wscript", "alt.vbs", inputFile.getAbsolutePath());
        Process msOfficeProcess = prBuilder.start();
        try {
            msOfficeProcess.waitFor();
            return emptyDocument;
        }
        finally {
            msOfficeProcess.destroy();
        }
    }    
}
