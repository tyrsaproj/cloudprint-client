/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cloudprint;

import Print.PrintInfoContainer;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;




public class FXMLDocumentController implements Initializable {
    private CloudPrint program;
    
    private File originalFile;
    private File convertedFile;

    @FXML // fx:id="newpane"
    private Pane newpane; // Value injected by FXMLLoader
    @FXML // fx:id="newpane"
    private Label statlabel; // Value injected by FXMLLoader
    @FXML // fx:id="button"
    private Button button1; // Value injected by FXMLLoader
   @FXML // fx:id="button"
    private Button options; // Value injected by FXMLLoader
   @FXML // fx:id="button"
    private Button button21; // Value injected by FXMLLoader
    @FXML // fx:id="droplabel"
    private Label droplabel; // Value injected by FXMLLoader
    @FXML // fx:id="droplabel"
    private Text newtext; // Value injected by FXMLLoader
    @FXML // fx:id="Back"
    private AnchorPane Back; // Value injected by FXMLLoader
    @FXML // fx:id="Pane1"
    private Pane Pane1; // Value injected by FXMLLoader
    @FXML // fx:id="printselector"
    private ComboBox<String> printselector; // Value injected by FXMLLoader
    @FXML
    void refreshButtonClick(ActionEvent event) throws IOException, ClassNotFoundException
    {
       // this.program.Connect(CloudPrint.con.resume);
        button21.setDisable(true);
        program.getPrinterListFromServer();
        button21.setDisable(false);
        
        //this.program.Disconnect(CloudPrint.status.shutDown);
    }
    @FXML
    void optionsButtonClick(ActionEvent event) throws IOException, Exception
    {
        CloudPrint.PA.start(new Stage());
    }
    @FXML        
    void OnDragDropped(DragEvent event) throws IOException {
// Для Драг'н'дропа
                Dragboard db = event.getDragboard();
                boolean success = false;
                if(originalFile != null && originalFile.exists()) {
                    originalFile.delete();
                }
                if(convertedFile != null && convertedFile.exists()) {
                    convertedFile.delete();
                }
                if (db.hasFiles()) {
                    success = true;
                    String fileName = null;
                    for (File file:db.getFiles()) {
                        System.out.println(file.getName());
                        droplabel.setText(file.getName());
                        Path pt =  Files.copy(file.toPath(), Paths.get(file.getName()), StandardCopyOption.REPLACE_EXISTING);
                        originalFile = pt.toFile();
                        originalFile.deleteOnExit();
                        if(file.getName().endsWith(".pdf") || file.getName().endsWith(".txt") ||
                                file.getName().endsWith(".jpg") || file.getName().endsWith(".jpeg") ||
                                file.getName().endsWith(".png") || file.getName().endsWith(".gif") ||
                                file.getName().endsWith(".PDF") || file.getName().endsWith(".TXT") ||
                                file.getName().endsWith(".JPG") || file.getName().endsWith(".JPEG") ||
                                file.getName().endsWith(".PNG") || file.getName().endsWith(".GIF")) {
                            this.program.setFileToPrint(originalFile);
                            fileName = file.getName();
                        } else {
                            try {
                                convertedFile = Converter.convert(originalFile);
                                convertedFile.deleteOnExit();
                                this.program.setFileToPrint(convertedFile);
                                fileName = convertedFile.getName();
                            }
                            catch(Exception e) {
                                System.out.println("Ошибка конвертации" + e.toString());
                            }
                        }
                        
                        break;
                    }
                    CloudPrint.PA.SetFileName(fileName);
                }
                event.setDropCompleted(success);
                event.consume();
    }
    
    @FXML
    void OnDragOver(DragEvent event) {
            
            
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
    }
    
    @FXML 
    void printButtonAction(ActionEvent event) throws IOException, UnknownHostException, ClassNotFoundException 
    { 
    // кнопка печать
      //  program.Connect(CloudPrint.con.resume);
        String query;
        for(PrintInfoContainer i: CloudPrint.GetPrinterList())
        {
            if( i.getPrinter() == printselector.getValue() )
            {
                query = printselector.getValue() + ":"+ i.getIp()+":"+ "OpenServer:" + i.getName();
                this.program.SendObject(query);
                program.setIP(i.getIp());
                program.setPrintContainer(i);
                break;
            }
        }
        this.Wazzup("Send");
        //program.Disconnect(CloudPrint.status.tmpDisconnect);
    }
    
    
    public void Wazzup(String str)
    {
        this.statlabel.setText("Status : " +str);   
    }
            
    
    public void RefreshPrinterList() {
        printselector.getItems().clear();
        for(PrintInfoContainer p : program.GetPrinterList()) {
            printselector.getItems().add(p.getPrinter());
        }
        printselector.setValue("Select Printer");
    }
    public void setProgram(CloudPrint program)
    {
        this.program = program;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        RefreshPrinterList();
    }

   


}