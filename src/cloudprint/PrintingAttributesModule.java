package cloudprint;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.*;
import javax.print.attribute.HashPrintRequestAttributeSet;

public class PrintingAttributesModule extends Application {
    
    private PrintRequestAttributeSet aset;
    private String fileName;
    
    public PrintRequestAttributeSet GetAset() {
        return aset;
    }
    
    public void SetFileName(String fileName) {
        if(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png") || fileName.endsWith(".gif")) {
            aset.remove(PageRanges.class);
        }
        this.fileName = fileName;
    }
    public PrintingAttributesModule() {
        fileName = "";
        aset = new HashPrintRequestAttributeSet();
        aset.add(Chromaticity.COLOR);
        aset.add(OrientationRequested.PORTRAIT);
        aset.add(MediaSizeName.ISO_A4);
        aset.add(new Copies(1));
    }
    
    @Override
    public void start(Stage AttrStage) throws Exception {
        PrintingAttributesWindowController controller = new PrintingAttributesWindowController(AttrStage, fileName, aset);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/cloudprint/PrintingAttributesWindow.fxml"));
        loader.setController(controller);
        Parent root = (Parent)loader.load();
        Scene scene = new Scene(root);
        AttrStage.setScene(scene);
        AttrStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
