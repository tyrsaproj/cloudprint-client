package cloudprint;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.*;
import javafx.stage.Stage;

public class PrintingAttributesWindowController implements Initializable {
    
    private PrintRequestAttributeSet aset;
    
    private Stage stage;
    
    private String fileName;
    
    private ObservableList<String> Numbers;
    
    private ObservableList<String> SizesOfPaper;
    
    @FXML
    private ComboBox SizeOfPaperBox;
    @FXML
    private ComboBox CountOfCopiesBox;
    @FXML
    private RadioButton PortraitOrientationRadioButton;
    @FXML
    private RadioButton LandscapeOrientationRadioButton;
    @FXML
    private RadioButton ColorPrintRadioButton;
    @FXML
    private RadioButton MonochromePrintRadioButton;
    @FXML
    private TitledPane CountOfPagesSettingTitledPane;
    @FXML
    private RadioButton AllPagesPrintRadioButton;
    @FXML
    private RadioButton IntervalPagesPrintRadioButton;
    @FXML
    private TextField StartIntervalPagesPrintTextField;
    @FXML
    private TextField FinishIntervalPagesPrintTextField;
    @FXML
    private RadioButton SelectedPagesPrintRadioButton;
    @FXML
    private TextField SelectingPagesPrintTextField;
    @FXML
    private Label ErrorLabel;
    @FXML
    private Button ContinueButton;
    
    @FXML
    void ContinueButtonAction(ActionEvent event) {
        aset.clear();
        
        if(ColorPrintRadioButton.isSelected()) {
            aset.add(Chromaticity.COLOR);
        } else if(MonochromePrintRadioButton.isSelected()) {
            aset.add(Chromaticity.MONOCHROME);
        }
        
        if(LandscapeOrientationRadioButton.isSelected()) {
            aset.add(OrientationRequested.LANDSCAPE);
        } else if(PortraitOrientationRadioButton.isSelected()) {
            aset.add(OrientationRequested.PORTRAIT);
        }
            
        switch(SizeOfPaperBox.getValue().toString()) {
            case "Letter": aset.add(MediaSizeName.NA_LETTER); break;
            case "Legal": aset.add(MediaSizeName.NA_LEGAL); break;
            case "Executive": aset.add(MediaSizeName.EXECUTIVE); break;
            case "A3": aset.add(MediaSizeName.ISO_A3); break;
            case "A4": aset.add(MediaSizeName.ISO_A4); break;
            case "B4": aset.add(MediaSizeName.ISO_B4); break;
            case "B5": aset.add(MediaSizeName.ISO_B5); break;
        }
        
        aset.add(new Copies(Integer.parseInt(CountOfCopiesBox.getValue().toString())));
        
        if(IntervalPagesPrintRadioButton.isSelected()) {
            aset.add(new PageRanges(StartIntervalPagesPrintTextField.getText() + "-" + FinishIntervalPagesPrintTextField.getText()));
        } else if(SelectedPagesPrintRadioButton.isSelected()) {
            aset.add(new PageRanges(SelectingPagesPrintTextField.getText()));
        }
        
        stage.close();
    }
    
    @FXML
    void CancelButtonAction(ActionEvent event) {
        stage.close();
    }
    
    @FXML
    private void ColorPrintRadioButtonAction(ActionEvent event) {
        ColorPrintRadioButton.setSelected(true);
        MonochromePrintRadioButton.setSelected(false);
    }
    
    @FXML
    private void MonochromePrintRadioButtonAction(ActionEvent event) {
        MonochromePrintRadioButton.setSelected(true);
        ColorPrintRadioButton.setSelected(false);
    }
    
    @FXML
    private void PortraitOrientationRadioButtonAction(ActionEvent event) {
        PortraitOrientationRadioButton.setSelected(true);
        LandscapeOrientationRadioButton.setSelected(false);
    }
    
    @FXML
    private void LandscapeOrientationRadioButtonAction(ActionEvent event) {
        LandscapeOrientationRadioButton.setSelected(true);
        PortraitOrientationRadioButton.setSelected(false);
    }
    
    @FXML
    private void AllPagesPrintRadioButtonAction(ActionEvent event) {
        AllPagesPrintRadioButton.setSelected(true);
        IntervalPagesPrintRadioButton.setSelected(false);
        StartIntervalPagesPrintTextField.setDisable(true);
        FinishIntervalPagesPrintTextField.setDisable(true);
        SelectedPagesPrintRadioButton.setSelected(false);
        SelectingPagesPrintTextField.setDisable(true);
        CheckError();
    }
    
    @FXML
    private void IntervalPagesPrintRadioButtonAction(ActionEvent event) {
        IntervalPagesPrintRadioButton.setSelected(true);
        StartIntervalPagesPrintTextField.setDisable(false);
        FinishIntervalPagesPrintTextField.setDisable(false);
        AllPagesPrintRadioButton.setSelected(false);
        SelectedPagesPrintRadioButton.setSelected(false);
        SelectingPagesPrintTextField.setDisable(true);
        CheckError();
        StartIntervalPagesPrintTextField.requestFocus();
    }
    
    @FXML 
    private void SelectedPagesPrintRadioButtonAction(ActionEvent event) {
        SelectedPagesPrintRadioButton.setSelected(true);
        SelectingPagesPrintTextField.setDisable(false);
        AllPagesPrintRadioButton.setSelected(false);
        IntervalPagesPrintRadioButton.setSelected(false);
        StartIntervalPagesPrintTextField.setDisable(true);
        FinishIntervalPagesPrintTextField.setDisable(true);
        CheckError();
        SelectingPagesPrintTextField.requestFocus();
    }
    
    @FXML
    private void TextFieldKeyReleased(KeyEvent event) { //Ивент для проверки правильности вводимых данных в поля
        CheckError();
    }
    
    @FXML
    private void StartIntervalPagesPrintTextFieldKeyPressed(KeyEvent event) { //Ивент для перемещения фокуса на второе поле после нажатия Enter
        if(event.getCode() == KeyCode.ENTER) {
            if(StartIntervalPagesPrintTextField.getStyle().contains("-fx-border-color: #FF0000")) {
                StartIntervalPagesPrintTextField.requestFocus();
            } else {
                FinishIntervalPagesPrintTextField.requestFocus();
            }
        }
    }
    
    @FXML
    private void FinishIntervalPagesPrintTextFieldKeyPressed(KeyEvent event) { //Ивент для перемещения фокуса на кнопку ContinueButton после нажатия Enter
        if(event.getCode() == KeyCode.ENTER) {
            if(FinishIntervalPagesPrintTextField.getStyle().contains("-fx-border-color: #FF0000")) {
                FinishIntervalPagesPrintTextField.requestFocus();
            } else if(StartIntervalPagesPrintTextField.getStyle().contains("-fx-border-color: #FF0000")) {
                FinishIntervalPagesPrintTextField.requestFocus();
            } else {
                ContinueButton.requestFocus();
            }
        }
    }
    
    @FXML
    private void SelectingPagesPrintTextFieldKeyPressed(KeyEvent event) {
        if(event.getCode() == KeyCode.ENTER) {
            if(SelectingPagesPrintTextField.getStyle().contains("-fx-border-color: #FF0000")) {
                SelectingPagesPrintTextField.requestFocus();
            } else {
                ContinueButton.requestFocus();
            }
        }
    }
    
    @FXML
    private void ContinueButtonKeyPressed(KeyEvent event) {
        if(event.getCode() == KeyCode.ENTER) {
            ContinueButtonAction(new ActionEvent());
        }
    }
    
    private void CheckError() { //Метод осуществляющий проверку содержания полей на допустимые символы и подсвечивающй их
        boolean errorFlag = false;
        if(!SelectingPagesPrintTextField.isDisabled()) {
            try {
                new PageRanges(SelectingPagesPrintTextField.getText());
                SelectingPagesPrintTextField.setStyle("-fx-border-color: transparent");
            } catch(IllegalArgumentException e) {
                SelectingPagesPrintTextField.setStyle("-fx-border-color: #FF0000");
                ErrorLabel.setText("Недопустимые символы");
                errorFlag = true;
            }
        } else {
            SelectingPagesPrintTextField.setStyle("-fx-border-color: transparent");
        }
        
        if(!FinishIntervalPagesPrintTextField.isDisabled()) {
            try {
               Integer.parseInt(FinishIntervalPagesPrintTextField.getText());   
               FinishIntervalPagesPrintTextField.setStyle("-fx-border-color: transparent");
            } catch(IllegalArgumentException e) {
               FinishIntervalPagesPrintTextField.setStyle("-fx-border-color: #FF0000");
               ErrorLabel.setText("Недопустимые символы");
               errorFlag = true;
            }
        } else {
            FinishIntervalPagesPrintTextField.setStyle("-fx-border-color: transparent");
        }
        
        if(!StartIntervalPagesPrintTextField.isDisabled()) {
            try {
               Integer.parseInt(StartIntervalPagesPrintTextField.getText());   
               StartIntervalPagesPrintTextField.setStyle("-fx-border-color: transparent");
            } catch(IllegalArgumentException e) {
               StartIntervalPagesPrintTextField.setStyle("-fx-border-color: #FF0000");
               ErrorLabel.setText("Недопустимые символы");
               errorFlag = true;
            }
        } else {
             StartIntervalPagesPrintTextField.setStyle("-fx-border-color: transparent");
        }
        
        if(errorFlag) {
            ErrorLabel.setVisible(true);
            ContinueButton.setDisable(true);
        } else {
            ErrorLabel.setVisible(false);
            ContinueButton.setDisable(false);
        }
    }
    
    public PrintingAttributesWindowController(Stage stage, String fileName, PrintRequestAttributeSet inaset) {
        this.stage = stage;
        this.fileName = fileName;
        aset = inaset;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String[] sizes = new String[] {"Letter", "Legal", "Executive", "A3", "A4", "B4", "B5"};
        SizesOfPaper = FXCollections.observableArrayList(sizes);
        String[] nums = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"};
        Numbers = FXCollections.observableArrayList(nums);
        SizeOfPaperBox.setItems(SizesOfPaper);
        CountOfCopiesBox.setItems(Numbers);
        
        if(aset.containsValue(Chromaticity.MONOCHROME)) {
            MonochromePrintRadioButton.setSelected(true);
        } else {
            ColorPrintRadioButton.setSelected(true);
        }
            
        if(aset.containsValue(OrientationRequested.LANDSCAPE)) {
            LandscapeOrientationRadioButton.setSelected(true);
        } else {
            PortraitOrientationRadioButton.setSelected(true);
        }
            
        if(aset.containsValue(MediaSizeName.NA_LETTER)) {
            SizeOfPaperBox.setValue("LETTER");
        } else if(aset.containsValue(MediaSizeName.NA_LEGAL)) {
            SizeOfPaperBox.setValue("LEGAL");
        } else if(aset.containsValue(MediaSizeName.EXECUTIVE)) {
            SizeOfPaperBox.setValue("EXECUTIVE");
        } else if(aset.containsValue(MediaSizeName.ISO_A3)) {
            SizeOfPaperBox.setValue("A3");
        } else if(aset.containsValue(MediaSizeName.ISO_A4)) {
            SizeOfPaperBox.setValue("A4");
        } else if(aset.containsValue(MediaSizeName.ISO_B5)) {
            SizeOfPaperBox.setValue("B5");
        } else {
            SizeOfPaperBox.setValue("A4");
        }
            
        for(int i = 1; i < 21; i++) {
            if(aset.containsValue(new Copies(i))) {
                CountOfCopiesBox.setValue(i);
                break;
            } else {
                CountOfCopiesBox.setValue(1);
            }
        }
        
        PageRanges pg = (PageRanges)aset.get(PageRanges.class);
        if(pg != null) {
            if(pg.toString().contains(",") || !pg.toString().contains("-")) {
                StartIntervalPagesPrintTextField.setDisable(true);
                FinishIntervalPagesPrintTextField.setDisable(true);
                SelectedPagesPrintRadioButton.setSelected(true);
                SelectingPagesPrintTextField.setText(pg.toString());
            } else {
                SelectingPagesPrintTextField.setDisable(true);
                IntervalPagesPrintRadioButton.setSelected(true);
                StartIntervalPagesPrintTextField.setText(pg.toString().split("-")[0]);
                FinishIntervalPagesPrintTextField.setText(pg.toString().split("-")[1]);
            }
        } else {
            StartIntervalPagesPrintTextField.setDisable(true);
            FinishIntervalPagesPrintTextField.setDisable(true);
            SelectingPagesPrintTextField.setDisable(true);
            AllPagesPrintRadioButton.setSelected(true);
        }
        
        if(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png") || fileName.endsWith(".gif") || fileName == "") {
            CountOfPagesSettingTitledPane.setDisable(true);
        } else {
            CountOfPagesSettingTitledPane.setDisable(false);
        }
        
        stage.setTitle("Параметры печати");
        stage.setResizable(false);
    }    
}
